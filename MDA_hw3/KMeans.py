from pyspark import SparkConf, SparkContext
import matplotlib.pyplot as plt 

MAX_ITER = 20 

def Euclidean_distance(A, B):
    dist_square = 0.0
    for a, b in zip(A, B):
        dist_square += (a - b) ** 2
    return (dist_square ** 0.5)

def Manhattan_distance(A, B):
    dist = 0.0
    for a ,b in zip(A, B):
        dist += abs(a - b)
    return dist




def mapper_gen_idx_value_pair(rowData_rowth):
    '''
        ('x0 x1 ... x9', rowth) -> (rowth, [x0, x1, ..., x9])
    '''
    rowData, rowth = rowData_rowth
    values = rowData.split(' ')
    return (rowth, tuple([float(e) for e in values]))


def mapper_cal_Euclidean_distance(PC_pair):
    point, centroid = PC_pair
    pth, p_val = point
    cth, c_val = centroid
    return ( pth, (cth, Euclidean_distance(p_val, c_val), p_val) )

def mapper_cal_Manhattan_distance(PC_pair):
    point, centroid = PC_pair
    pth, p_val = point
    cth, c_val = centroid
    return ( pth, (cth, Manhattan_distance(p_val, c_val), p_val) )

def find_closest_centroid(point):
    pth, centroids_dist = point
    closest_cth = float("inf") 
    closest_dist = float("inf")
    p_val = tuple()
    for cth, dist, p_val_d in centroids_dist:
        if dist < closest_dist:
            closest_cth = cth
            closest_dist = dist
            p_val = p_val_d
    return (closest_cth, (closest_dist, p_val))

def cal_mean_vec(cluster):
    cth, dist_and_p_vals = cluster
    num_p = 0
    p_val_sum = (0.,) * 58
    for dist, p_val in dist_and_p_vals:
        p_val_sum = tuple([a+b for a, b in zip(p_val_sum, p_val)])
        num_p += 1
    return (cth, tuple([x/num_p for x in p_val_sum]))



# init Spark
SparkContext.setSystemProperty('spark.executor.memory', '10g')
conf = SparkConf().setMaster('local[*]').setAppName('KMeans')
sc = SparkContext(conf=conf)


# read data points 
Points = sc.textFile('./testcase/data.txt').zipWithIndex()
Points = Points.map(mapper_gen_idx_value_pair)




def KMeans_procedure(centroid_filename, mapper_cal_distance):
    Centroids = sc.textFile(centroid_filename).zipWithIndex()
    Centroids = Centroids.map(mapper_gen_idx_value_pair)
    
    iteration_list = list()
    cost_list = list()

    for i in range(MAX_ITER):
        #  print(f'iter: {i:2d}')
        # assign points to the cluster with the closest centroid
        cartesian_product = Points.cartesian(Centroids)
        points_centroids_dist = cartesian_product.map(mapper_cal_distance)
        dist_to_centroids = points_centroids_dist.groupByKey()
        closest_centroid = dist_to_centroids.map(find_closest_centroid)

        # cal cost 
        cost = closest_centroid.map(lambda x: x[1][0]).reduce(lambda x,y: x+y)
        cost_list.append(cost)
        iteration_list.append(i)
        
        # Recompute the centroid of each cluster
        new_centroids = closest_centroid.groupByKey().map(cal_mean_vec)
        t = new_centroids.collect()
        Centroids = sc.parallelize(t)

    return Centroids.collect(), cost_list, iteration_list




centroid_list_Ec1, cost_list_Ec1, iter_list_Ec1 = KMeans_procedure('./testcase/c1.txt', mapper_cal_Euclidean_distance)
centroid_list_Ec2, cost_list_Ec2, iter_list_Ec2 = KMeans_procedure('./testcase/c2.txt', mapper_cal_Euclidean_distance)
centroid_list_Ec1 = [x[1] for x in centroid_list_Ec1]    
centroid_list_Ec2 = [x[1] for x in centroid_list_Ec2]    


plt.plot(iter_list_Ec1, cost_list_Ec1, label='Euclidean, c1')
plt.plot(iter_list_Ec2, cost_list_Ec2, label='Euclidean, c2')
plt.legend()
plt.title('Euclidean')
plt.show()


print(f'Euclidean c1, percentage improvement: {(cost_list_Ec1[0] - cost_list_Ec1[-1]) / cost_list_Ec1[0] * 100}%')
print(f'Euclidean c2, percentage improvement: {(cost_list_Ec2[0] - cost_list_Ec2[-1]) / cost_list_Ec2[0] * 100}%')


with open('./Ec1_calByE.csv', 'w') as f:
    f.write(f'Euclidean,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Euclidean_distance(centroid_list_Ec1[i], centroid_list_Ec1[j])}')
            j += 1
        f.write('\n')

with open('./Ec1_calByM.csv', 'w') as f:
    f.write(f'Manhattan,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Manhattan_distance(centroid_list_Ec1[i], centroid_list_Ec1[j])}')
            j += 1
        f.write('\n')

with open('./Ec2_calByE.csv', 'w') as f:
    f.write(f'Euclidean,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Euclidean_distance(centroid_list_Ec2[i], centroid_list_Ec2[j])}')
            j += 1
        f.write('\n')

with open('./Ec2_calByM.csv', 'w') as f:
    f.write(f'Manhattan,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Manhattan_distance(centroid_list_Ec2[i], centroid_list_Ec2[j])}')
            j += 1
        f.write('\n')




centroid_list_Mc1, cost_list_Mc1, iter_list_Mc1 = KMeans_procedure('./testcase/c1.txt', mapper_cal_Manhattan_distance)
centroid_list_Mc2, cost_list_Mc2, iter_list_Mc2 = KMeans_procedure('./testcase/c2.txt', mapper_cal_Manhattan_distance)
centroid_list_Mc1 = [x[1] for x in centroid_list_Mc1]    
centroid_list_Mc2 = [x[1] for x in centroid_list_Mc2]    

plt.plot(iter_list_Mc1, cost_list_Mc1, label='Manhattan, c1')
plt.plot(iter_list_Mc2, cost_list_Mc2, label='Manhattan, c2')
plt.legend()
plt.title('Manhattan')
plt.show()

print(f'Manhattan c1, percentage improvement: {(cost_list_Mc1[0] - cost_list_Mc1[-1]) / cost_list_Mc1[0] * 100}%')
print(f'Manhattan c2, percentage improvement: {(cost_list_Mc2[0] - cost_list_Mc2[-1]) / cost_list_Mc2[0] * 100}%')


with open('./Mc1_calByE.csv', 'w') as f:
    f.write(f'Euclidean,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Euclidean_distance(centroid_list_Mc1[i], centroid_list_Mc1[j])}')
            j += 1
        f.write('\n')

with open('./Mc1_calByM.csv', 'w') as f:
    f.write(f'Manhattan,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Manhattan_distance(centroid_list_Mc1[i], centroid_list_Mc1[j])}')
            j += 1
        f.write('\n')

with open('./Mc2_calByE.csv', 'w') as f:
    f.write(f'Euclidean,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Euclidean_distance(centroid_list_Mc2[i], centroid_list_Mc2[j])}')
            j += 1
        f.write('\n')

with open('./Mc2_calByM.csv', 'w') as f:
    f.write(f'Manhattan,1,2,3,4,5,6,7,8,9,10\n')
    for i in range(10):
        f.write(f'{i+1}' + ','*i)
        j = i
        while j < 10:
            f.write(f',{Manhattan_distance(centroid_list_Mc2[i], centroid_list_Mc2[j])}')
            j += 1
        f.write('\n')






sc.stop()



