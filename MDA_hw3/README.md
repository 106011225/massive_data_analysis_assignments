# Massive data analysis: K-means


## Big data

> Big data refers to data sets that are too large or complex to be dealt with by traditional data-processing application software. - Wikipedia


## Apache Spark

> Apache Spark is an open-source unified analytics engine for large-scale data processing. Spark provides an interface for programming clusters with implicit data parallelism and fault tolerance. - Wikipedia


## Goal

- Write a [program](./KMeans.py) with Spark (Python) to implement *Iterative K-Means Algorithm* 
- Compare the results with different distance metrics and initialization strategies
    - Euclidean distance or Manhattan distance
    - Random initial cluster centroids or centroids that are as far apart as possible


## Usage

1. Install `pyspark`
    ```
    $ pip install pyspark==3.1.2
    ```

2. Run [KMeans.py](./KMeans.py)
    ```
    $ python KMeans.py
    ```

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**

