# Massive data analysis: Recommendation system (item-item collaborative filtering)

Note: This is a group assignment with 2 members in total.


## Big data

> Big data refers to data sets that are too large or complex to be dealt with by traditional data-processing application software. - Wikipedia


## Apache Spark

> Apache Spark is an open-source unified analytics engine for large-scale data processing. Spark provides an interface for programming clusters with implicit data parallelism and fault tolerance. - Wikipedia


## Item-item collaborative filtering

> Item-item collaborative filtering, or item-based, or item-to-item, is a form of collaborative filtering for recommender systems based on the similarity between items calculated using people's ratings of those items. Item-item collaborative filtering was invented and used by Amazon.com in 1998. - Wikipedia


## Goal 

- Given a **sparse** *Utility Matrix* where each column represents a user, each row represents a movie and each entry is the rating rated by the user, predict the unknown ratings
- This is done with 2 steps
    1. Calculate the *cosine similarity* of all movies pairs with subtract mean
    2. Predict the unknown ratings using *item-item collaborative filtering*
- The dataset contains 610 users and 9742 movies

<img src="./assets/item_item_CF.png" width="400"><br>
(Picture cited from https://web.stanford.edu/class/cs246/slides/07-recsys1.pdf)  

Please refer to the *Question 2* in [SPEC.pdf](./SPEC.pdf) for more details


## Usage

1. Please refer to [Term_Project.ipynb](./Term_Project.ipynb)


