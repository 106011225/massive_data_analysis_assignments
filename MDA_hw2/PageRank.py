from pyspark import SparkConf, SparkContext

beta = 0.8


def mapper_raw_2_SrcDestPair(line):
    '''
    transform: "1    2" -> (1, 2)
    '''
    v_src, v_dest = line.split('\t')
    return (int(v_src), int(v_dest))


def mapper_count_numDest(row):
    '''
    transform: (1, [2, 3]) -> (1, {'out_deg': 2, 'dests': [2, 3]})
    '''
    return ( row[0], 
             {'out_deg': len(row[1]), 'dests': row[1]} )


def mapper_gen_vertices(line):
    '''
    transform: "1    2" -> [(1, 0), (2, 0)]
    '''
    v1 , v2 = line.split('\t')
    return [(int(v1), 0), (int(v2), 0)]


def mapper_cal_RnextContribution(data):
    '''
    transform: (1, (vertex_val, {'out_deg': 2, 'dests': [2, 3]})))
               -> [(2, vertex_val/2), (3, vertex_val/2)]
    '''
    contributions = []
    vertex_val = data[1][0]
    vertex_outInfo = data[1][1]
    if vertex_outInfo != None:
        out_val = beta * (vertex_val / vertex_outInfo['out_deg'])
        for dest in vertex_outInfo['dests']:
            contributions.append( (dest, out_val) )
    return contributions



# init Spark
SparkContext.setSystemProperty('spark.executor.memory', '10g')
conf = SparkConf().setMaster('local[*]').setAppName('PageRank')
sc = SparkContext(conf=conf)


#  raw = sc.textFile('./input-test.txt')
raw = sc.textFile('./input.txt')

edges = raw.map(mapper_raw_2_SrcDestPair)
table = edges.groupByKey()
table = table.map(mapper_count_numDest)

vertices = raw.flatMap(mapper_gen_vertices)
vertices = vertices.reduceByKey(lambda a, b: a + b)
N = vertices.count()
r_curr = vertices.map(lambda vertex: (vertex[0], 1/N))



for i in range(20):
    X = r_curr.leftOuterJoin(table)

    r_next = X.flatMap(mapper_cal_RnextContribution)
    r_next = r_next.union(vertices)
    r_next = r_next.reduceByKey(lambda a, b: a+b)

    S = r_next.map(lambda x: x[1]).sum()
    renormalize_val = (1-S) / N
    r_next = r_next.map(lambda x: (x[0], x[1] + renormalize_val) )

    r_curr = r_next




Ans = r_curr.sortBy(lambda x: x[0]).sortBy(lambda x: x[1], ascending=False).collect()


with open('./Outputfile.txt', 'w') as f:
    for x in Ans:
        f.write(f'{x[0]:d}\t{x[1]:.8f}\n')

sc.stop()



