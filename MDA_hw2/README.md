# Massive data analysis: PageRank


## Big data

> Big data refers to data sets that are too large or complex to be dealt with by traditional data-processing application software. - Wikipedia


## Apache Spark

> Apache Spark is an open-source unified analytics engine for large-scale data processing. Spark provides an interface for programming clusters with implicit data parallelism and fault tolerance. - Wikipedia


## PageRank

> "PageRank (PR) is an algorithm used by Google Search to rank web pages in their search engine results. It is named after both the term "web page" and co-founder Larry Page. PageRank is a way of measuring the importance of website pages." - Wikipedia

![img](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/PageRanks-Example.svg/330px-PageRanks-Example.svg.png)


## Goal 

- Write a [program](./PageRank.py) with Spark (Python) to solve the PageRank problem
- Given G = (V,E), report top 10 vertices and their PageRanks. 
- There are 10876 vertices (Webpages) and 39994 edges (Hyperlinks)


## Usage

1. Install `pyspark`
    ```
    $ pip install pyspark==3.1.2
    ```

2. Run [PageRank.py](./PageRank.py)
    ```
    $ python PageRank.py
    ```

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**

