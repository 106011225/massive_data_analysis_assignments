# Massive data analysis: Matrix multiplication


## Big data

> Big data refers to data sets that are too large or complex to be dealt with by traditional data-processing application software. - Wikipedia


## Apache Spark

> Apache Spark is an open-source unified analytics engine for large-scale data processing. Spark provides an interface for programming clusters with implicit data parallelism and fault tolerance. - Wikipedia


## Goal 

- Write a [program](./hw1_106011225.py) with Spark (Python) to solve the matrix multiplication problem
- The size of input matrices are 500x500


## Usage

1. Install `pyspark`
    ```
    $ pip install pyspark==3.1.2
    ```

2. Run [hw1_106011225.py](./hw1_106011225.py)
    ```
    $ python hw1_106011225.py
    ```


**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**

