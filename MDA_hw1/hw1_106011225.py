from pyspark import SparkConf, SparkContext
import time

init_time = time.time()

def mapper_M(entryInfo):
    '''
    transform: 'M,i,j,m_ij' -> (j, (M, i, m_ij))
    E.g.: 'M,0,0,10' -> (0, ('M', 0, 10))
    '''
    entry = entryInfo.split(',')
    i, j, value = entry[1:4]
    return (int(j), ('M', int(i), int(value)))

def mapper_N(entryInfo):
    '''
    transform: 'N,j,k,n_jk' -> (j, (N, k, n_jk))
    E.g.: 'N,0,0,1' -> (0, ('N', 0, 1))
    '''
    entry = entryInfo.split(',')
    j, k, value = entry[1:4]
    return (int(j), ('N', int(k), int(value)))


def mapper_mul(entryPair):
    '''
    transform: (j, ((M, i, m_ij), (N, k, n_jk))) -> ((i, k), m_ij*n_jk) 
    E.g.: (0, (('M', 0, 10), ('N', 0, 1))) -> ((0, 0), 10) 
    '''
    m = entryPair[1][0]
    n = entryPair[1][1]
    return ((m[1], n[1]), m[2] * n[2]) 



# init Spark
SparkContext.setSystemProperty('spark.executor.memory', '8g')
conf = SparkConf().setMaster('local[*]').setAppName('matrix_mul')
sc = SparkContext(conf=conf)

# create text file RDD
raw = sc.textFile('./input.txt')

# split out raw data of M & N
M = raw.filter(lambda x: x[0] == 'M')
N = raw.filter(lambda x: x[0] == 'N')

# 'M,0,0,10' -> ('0', ('M', '0', '10'))
M = M.map(mapper_M)
N = N.map(mapper_N)

# get all m_ij, n_jk pairs
X = M.join(N)


# mul each m_ij, n_jk pairs
X = X.map(mapper_mul)

# sum up
X = X.reduceByKey(lambda a, b: a + b)

# sort by index
X = X.sortByKey()

# output
X.saveAsTextFile(f'Output_{time.time()}')

# terminate
sc.stop()
print(f'used time: {time.time() - init_time}')
